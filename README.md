# GetRecd

A simple Discord bot for use on a local server.
Will listen in to an audio channel and record the output.

## Getting started

To get started with GetRecd, you will need a few things

* You need to make a Discord bot and add it to your server.
  While generating the Oauth2 link to add the bot to your server,
  check the "bot" box in the top window and then check all of the
  individual permissions in the bottom except for "Administrator".
  Later, if you run the bot script and get an error about missing permissions,
  you can create an administrator role and add it to the bot.
  I currently have *all* the permissions added to my bot, which is bad practice.
  If you figure out another way, let me know.

* You need to configure a file called .settings.json with all your project details.
  Here's an example:
  ```
  {
    "permissions_int" : 1234,           # configured through Discord dev portal
    "bot_token" : "TOKEN",              # found in Discord dev portal, Bot tab
    "OAuth2_ID" : 4567,                 # in app view, OAuth2 tab, as "CLIENT ID"
    "public_key" : "KEY",               # in app view, Bot tab
    "guild_ids"  : [1111, 2222],        # The ID of your Discord server(s)
    "libopus_dir" : "/usr/lib/x86_64-linux-gnu/libopus.so",  # you must install libopus and put the location here
    "archive" : "/some/directory/for/storage"  # Not currently used. Will be a dir for storing the sound files.
  }
  ```

* You need a python environment with all the correct libraries installed.
  You must have Python3.8 or later. Once you have that taken care of, simply
  run `pip install -r requirements.txt`. After that you should be able to run the bot.