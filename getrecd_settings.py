from json import (loads, dumps)

def get_bot_settings(settings_file=".settings.json"):
    settings = {}
    settings_str = ""
    with open(settings_file) as sfile:
        settings_str = sfile.read()
    settings = loads(settings_str)

    return settings