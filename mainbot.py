import discord
from discord.ext import commands
from discord.commands import slash_command
from getrecd_settings import get_bot_settings
import soundfile as sf
import wave
import numpy as np
import datetime
import time
import os

# Default encoding. For now we will stick with one format.
# Later we might let the user choose. Ideally this will be
# "mp3", as we are just recording voice and mp3 will take up
# the least amount of space.
ENCODING = "wav"
SETTINGS = {}

# Load in the settings file upfront
# Expected keys are those needed for the bot to function.
# Adjust them as needed for your project.
SETTINGS_EXPECTED_KEYS = [
    "permissions_int", "bot_token", "OAuth2_ID",
    "public_key", "guild_ids", "libopus_dir"
]
SETTINGS = get_bot_settings()
value_errors = []
# If any expected settings are missing, the list of missing keys will print
# to console when script fails.
for key in SETTINGS_EXPECTED_KEYS:
    if key not in list(SETTINGS.keys()):
        value_errors.append(key)
if value_errors:
    raise ValueError("GetRecd encountered an error: .settings.json file is missing keys [{}]".format(value_errors))


bot = discord.Bot(guilds=SETTINGS["guild_ids"], application_id=SETTINGS["OAuth2_ID"])
bot.connections = {}
bot.archives = {}

@bot.event
async def on_ready():
    print("GetRecd online!")

@bot.command()
# TODO: Figure out writing to local storage with non-wav encoding options
# @discord.option(
#     "encoding",
#     choices=[
#         "mp3",
#         "wav",
#         "pcm",
#         "ogg",
#         "mka",
#         "mkv",
#         "mp4",
#         "m4a",
#     ],
# )
async def record_voice_channel(ctx: discord.ApplicationContext):
    """
    NOTE: Admin-only. Record on your currently active voice channel.
    """
    # If the archive directory has been provided, make sure there is
    # a voice archive for today's date.
    archive = {}
    if "archive" in list(SETTINGS.keys()):
        archive["root"] = SETTINGS["archive"]
        # Set up directory name for today's date
        archive["dir"] = archive["root"] + "/" + datetime.date.today().isoformat()
        if not os.path.isdir(archive["root"]):
            print("Archive directory {} not found. Creating it...".format(archive["root"]))
            try:
                os.mkdir(archive["root"])
            except:
                print("Could not make directory {}. Will use current directory.".format(archive["root"]))
                # clear the archive dictionary.
                archive = {}
        # if archive is not empty, the root directory exists 
        if archive and not os.path.isdir(archive["dir"]):
            # Try to make the directory for today.
            print("Creating archive for today's date: {}".format(archive["dir"]))
            try:
                os.mkdir(archive["dir"])
            except:
                print("Could not make directory {}. Will use current directory.".format(archive["root"]))
                # clear the archive dictionary.
                archive = {}

    # First we need to 
    voice = ctx.author.voice
    # author is a Member
    author = ctx.author
    # author Guild is the current guild
    guild = author.guild

    # This can only work if we are in a voice channel
    if not voice:
        return await ctx.respond("You're not in a VC right now")
    # Disallow anyone but the guild owner from using this command
    if author is not guild.owner:
        return await ctx.respond("I'm sorry. Only {} may invoke this command.".format(guild.owner.display_name))
    encoding = ENCODING
    vc = await voice.channel.connect()
    bot.connections.update({ctx.guild.id: vc})
    bot.archives.update({ctx.guild.id: archive})

    if encoding == "mp3":
        sink = discord.sinks.MP3Sink()
    elif encoding == "wav":
        sink = discord.sinks.WaveSink()
    elif encoding == "pcm":
        sink = discord.sinks.PCMSink()
    elif encoding == "ogg":
        sink = discord.sinks.OGGSink()
    elif encoding == "mka":
        sink = discord.sinks.MKASink()
    elif encoding == "mkv":
        sink = discord.sinks.MKVSink()
    elif encoding == "mp4":
        sink = discord.sinks.MP4Sink()
    elif encoding == "m4a":
        sink = discord.sinks.M4ASink()
    else:
        return await ctx.respond("Invalid encoding.")

    vc.start_recording(
        sink,
        finished_callback,
        ctx.channel,
    )

    await ctx.respond("The recording has started!")


async def finished_callback(sink, channel: discord.TextChannel, *args):
    recorded_users = [f"<@{user_id}>" for user_id, audio in sink.audio_data.items()]
    # Get the VC guiild id (used to search for an archive)
    guild_id = sink.vc.guild.id
    await sink.vc.disconnect()
    # Get a timestamp for the file names
    lt = time.localtime()
    ts_str = "_{}.{}.{}".format(lt.tm_hour, lt.tm_min, lt.tm_sec)
    # Set archive_dir if we have an archive for this guild.
    # Add / to the end so we place the file inside the archive_dir
    archive_dir = "" if not bot.archives[guild_id] else bot.archives[guild_id]["dir"] + "/"
    # Get an array of files, and name each after the user archive_dir + display_name + ts_str
    files = []
    for user_id, audio in sink.audio_data.items():
        # try to get user display_name, if possible
        usr = await bot.fetch_user(user_id)
        usr_str = str(user_id) if not usr else usr.display_name
        files.append(discord.File(audio.file, f"{archive_dir + usr_str + ts_str}.{sink.encoding}"))
    # Write out all the files to local storage. Then seek back to the beginning so they can be posted to Discord
    for f in files:
        # Reset the buffer seek position,
        # so that it reads from the start of the file
        f.fp.seek(0)
        data = f.fp
        # Open buffer 'File-like Object'
        # and write the appropriate audio data to it
        if sink.encoding == "wav":
            with wave.open(data, "wb") as wf:
                wf.setnchannels(sink.vc.decoder.CHANNELS)
                wf.setsampwidth(sink.vc.decoder.SAMPLE_SIZE // sink.vc.decoder.CHANNELS)
                wf.setframerate(sink.vc.decoder.SAMPLING_RATE)
        # Reset buffer seek position again 
        # (not sure this is neccessary, but used for safety)
        data.seek(0)
        audio_buffer = data.read()
        audio_array = np.frombuffer(audio_buffer, dtype=np.int32, count=-1)
        sf.write(f.filename, audio_array, sink.vc.decoder.SAMPLING_RATE)
        # Seek to beginning so the files also get posted on Discord.
        f.fp.seek(0)
    await channel.send(f"Finished! Recorded audio for {', '.join(recorded_users)}.", files=files)


@bot.command()
async def stop(ctx: discord.ApplicationContext):
    """
    NOTE: Admin-only. Stop recording on currently active voice channel.
    """
    # author is a Member
    author = ctx.author
    # author Guild is the current guild
    guild = author.guild
    # Disallow anyone but the guild owner from using this command
    if author is not guild.owner:
        return await ctx.respond("I'm sorry. Only {} may invoke this command.".format(guild.owner.display_name))
    if ctx.guild.id in bot.connections:
        vc = bot.connections[ctx.guild.id]
        vc.stop_recording()
        del bot.connections[ctx.guild.id]
        await ctx.delete()
    else:
        await ctx.respond("Not recording in this guild.")

# Run the bot when it's invoked from the commandline.
if __name__ == "__main__":
    # Need to load Opus for the recording to work
    discord.opus.load_opus(SETTINGS["libopus_dir"])
    if not discord.opus.is_loaded():
        raise RunTimeError('Opus failed to load')
    
    # Normal operation
    bot.run(SETTINGS["bot_token"])